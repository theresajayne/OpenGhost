// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BasePlayerState.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerStart.h"
#include "TeamPlayerStart.generated.h"

/**
 * 
 */
UCLASS()
class OPENGHOST_API ATeamPlayerStart : public APlayerStart
{
	GENERATED_BODY()

public:

	ATeamPlayerStart(const FObjectInitializer& ObjectInitializer);

	virtual void Reset() override;

	UFUNCTION(BlueprintCallable)
	bool IsOccupied();

private:

	void OccupationQuery();
	void InvalidateQuery();

public:
	UPROPERTY(EditInstanceOnly, Category = Team)
	ETeamID myAllowedTeam;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Team)
	bool myIsOccupied;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Team)
	float myOccupationQueryTimeout;

private:

	UPROPERTY()
	bool myIsDirtyFlag;
	FTimerHandle myQueryTimerHandle;
};
