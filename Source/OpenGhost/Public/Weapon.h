#pragma once

#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

class ABaseCharacter;
class AProjectile;
class UAnimMontage;
class USceneComponent;
class UTexture2D;

UCLASS()
class OPENGHOST_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	AWeapon();
	virtual void Tick(float DeltaTime) override;
	virtual void Reset() override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	void Fire(const FVector& TargetLocation);
	void StartReload();
	void InterruptReload();
	bool CanReload();
	bool CanFire();

	UFUNCTION(NetMulticast, Reliable, Category = Weapon)
	void Multicast_Equip();

	void SetWielder(ABaseCharacter* Wielder);
	void Detach();

protected:
	virtual void BeginPlay() override;

private:

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Fire(const FVector& TargetLocation);

	void SpawnFireEffects();
	void SpawnProjectile(const FVector& StartLocation, const FVector& Direction);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_SpawnProjectile(const FVector& StartLocation, const FVector& Direction);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_StartReload();

	void UpdateReloadTimer(float DeltaTime);
	void FinishReload();
	void PlayMontageOnWielder(UAnimMontage* Montage, bool PlayOnFPMesh);

	void DelayedEquip();
	void Equip();

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	TSubclassOf<AProjectile>  myProjectileClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	UTexture2D* myCrosshair;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio)
	USoundBase* myFireSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio)
	USoundBase* myEmptyMagazineSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	FTransform myFPAttachOffset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	FTransform myTPAttachOffset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
	UAnimMontage* myFPFireAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
	UAnimMontage* myTPFireAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
	UAnimMontage* myFPReloadAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
	UAnimMontage* myTPReloadAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
	UAnimMontage* myFPReloadChamberedAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
	UAnimMontage* myTPReloadChamberedAnimation;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Mesh)
	USkeletalMeshComponent* myMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Mesh)
	USkeletalMeshComponent* myTPMesh;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USceneComponent* myMuzzleLocationComponent;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Weapon)
	int myAmmunition;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	int myMagazineSize;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Weapon)
	int myMagazineCount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	int myMaxMagazines;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Weapon)
	bool myHasRoundChambered;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	float myReloadTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	float myReloadTimeChambered;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Weapon)
	bool myIsReloading;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Weapon)
	float myReloadTimer;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Weapon)
	ABaseCharacter* myWielder;
};
