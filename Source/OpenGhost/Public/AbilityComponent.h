#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AbilityComponent.generated.h"

class ABaseCharacter;

UCLASS( ClassGroup=(Custom), Blueprintable, BlueprintType, meta=(BlueprintSpawnableComponent) )
class OPENGHOST_API UAbilityComponent : public UActorComponent
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAbilityActivatedDelegate);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAbilityDeactivatedDelegate);

public:	
	UAbilityComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void InitializeComponent() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable, Category = Ability)
	void TriggerAbility();

	UFUNCTION(BlueprintCallable, Category = Ability)
	void ActivateAbility();

	UFUNCTION(BlueprintImplementableEvent, Category = Ability)
	void AbilityActivatedEvent();

	UFUNCTION(BlueprintCallable, Category = Ability)
	void DeactivateAbility();

	UFUNCTION(BlueprintImplementableEvent, Category = Ability)
	void AbilityDeactivatedEvent();

	UFUNCTION(BlueprintCallable, Category = Character)
	ABaseCharacter* GetOwningCharacter();

	UFUNCTION(BlueprintCallable, Category = Ability)
	bool IsAbilityActive() const;

protected:
	virtual void BeginPlay() override;
	virtual void UpdateAbility(float DeltaTime);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Activate();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Deactivate();

private:
	bool CanActivate() const;
	void UpdateCooldownTimer(float DeltaTime);
	void UpdateActiveTimer(float DeltaTime);
	bool HasAuthority();

protected:

	// Cost for activating this ability
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ability)
	float myCost;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ability)
	float myOngoingCost;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ability)
	float myActiveTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ability)
	float myCooldown;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ability)
	bool myIsOngoing;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Ability)
	float myActiveTimer;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Ability)
	float myCooldownTimer;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Ability)
	bool myIsActive;

private:
	ABaseCharacter* myOwner;
	FAbilityActivatedDelegate myOnAbilityActivated;
	FAbilityDeactivatedDelegate myOnAbilityDeactivated;
};
