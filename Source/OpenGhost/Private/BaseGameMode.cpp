// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseGameMode.h"

#include "GameFramework/GameSession.h"
#include "BaseCharacter.h"
#include "BaseGameState.h"
#include "BasePlayerController.h"
#include "BasePlayerState.h"
#include "EngineUtils.h"
#include "OnlineGameInstance.h"
#include "OnlineSessionInterface.h"
#include "OnlineSessionSettings.h"
#include "OnlineSubsystem.h"
#include "TeamPlayerStart.h"

ABaseGameMode::ABaseGameMode()
	: myDetailedMatchState(EMatchState::Invalid)
{
	bDelayedStart = true;
}

void ABaseGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

}

void ABaseGameMode::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	GetWorldTimerManager().SetTimer(myMatchTimerHandle, this, &ABaseGameMode::MatchTimer, GetWorldSettings()->GetEffectiveTimeDilation(), true);

	//TODO myModeConfig.LoadConfig();

	if (ABaseGameState* gs = GetGameState<ABaseGameState>())
		gs->myModeConfig = myModeConfig;
}

void ABaseGameMode::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void ABaseGameMode::MatchTimer()
{
	if(ABaseGameState* const gs = Cast<ABaseGameState>(GameState))
	{
		if (!gs->myIsTimerPaused && myDetailedMatchState != EMatchState::Warmup)
		{
			if (gs->myRemainingTime > 0)
				gs->myRemainingTime--;
			//else TODO add debug feedback
			//	we are being delayed for some reason

			if (gs->myRemainingTime <= 0)
				EndCurrentDetailedMatchState();
		}
		else if (myDetailedMatchState == EMatchState::Warmup && ReadyToStartMatch())
			StartMatch();
	}
}

bool ABaseGameMode::ReadyToStartMatch()
{
	int32 availablePlayers = 0;	
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ABasePlayerController* controller = Cast<ABasePlayerController>(*It);
		ABasePlayerState* playerState = Cast<ABasePlayerState>(controller->PlayerState);

		if (!playerState->bIsSpectator)
			availablePlayers++;
	}

	return availablePlayers >= GetMinimumRequiredPlayers();
}

bool ABaseGameMode::ReadyToEndMatch()
{
	return true;
}

bool ABaseGameMode::CanContinueMatch()
{
	int32 teamAPlayers = 0;
	int32 teamBPlayers = 0;

	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ABasePlayerController* controller = Cast<ABasePlayerController>(*It);
		ABasePlayerState* playerState = Cast<ABasePlayerState>(controller->PlayerState);

		ETeamID team = playerState->GetTeam();
		if (myModeConfig.myIsTeamBased)
		{
			if (team == ETeamID::Team1)
				teamAPlayers;
			else if (team == ETeamID::Team2)
				teamBPlayers;
		}
		else
		{
			if (team == ETeamID::None)
				teamAPlayers;
		}
	}

	if (myModeConfig.myIsTeamBased && teamAPlayers > 0 && teamBPlayers > 0 && teamAPlayers + teamBPlayers >= GetMinimumRequiredPlayers())
		return true;
	else if (!myModeConfig.myIsTeamBased && teamAPlayers > GetMinimumRequiredPlayers())
		return true;
	return false;
}

int32 ABaseGameMode::GetMinimumRequiredPlayers()
{
	int32 minPlayers = -1;
	IOnlineSessionPtr sessions = IOnlineSubsystem::Get()->GetSessionInterface();
	FOnlineSessionSettings* currentSettings = sessions->GetSessionSettings(GameSessionName);
	if (!currentSettings)	//no session found
	{
		UE_LOG(LogActor, Warning, TEXT("Couldn't find a session."));
		return 1;
	}
	FOnlineSessionSetting* minPlayersSetting = currentSettings->Settings.Find(*Constants::SESSION_SETTING_MIN_PLAYERS);
	if (minPlayersSetting)
		minPlayersSetting->Data.GetValue(minPlayers);
	else
	{
		UE_LOG(LogActor, Warning, TEXT("Couldn't find minimum player setting."));
		return 1;
	}
	return minPlayers;
}

void ABaseGameMode::DetermineMatchWinner()
{
	if (myModeConfig.myWinCondition == EWinCondition::Points)
	{
		if (myModeConfig.myIsTeamBased)
			GetWinningTeamByPoints();
		else
			GetWinningPlayerByPoints();
	}
	else if (myModeConfig.myWinCondition == EWinCondition::Rounds)
	{
		if (myModeConfig.myIsTeamBased)
			GetWinningTeamByRounds();
	}

	ABaseGameState* gs = GetGameState<ABaseGameState>();
	if (gs && myWinningPlayers.Num() > 0)
		gs->myMatchWinner = myWinningPlayers[0];
}

void ABaseGameMode::DetermineRoundWinner()
{
	if (myModeConfig.myWinCondition == EWinCondition::Points)
	{
		if (myModeConfig.myIsTeamBased)
			GetWinningTeamByPoints();
		else
			GetWinningPlayerByPoints();

		FinishMatch();
	}
	else if (myModeConfig.myWinCondition == EWinCondition::Rounds)
	{
		ETeamID winner = ETeamID::Invalid;
		//TODO check for other win conditions (like bomb plants/defuse)
		ETeamID deadTeam = IsATeamDead();
		if (deadTeam == ETeamID::Team1)
			winner = ETeamID::Team2;
		else if (deadTeam == ETeamID::Team2)
			ETeamID::Team1;
		else
			winner = myModeConfig.myDefaultWinTeam;

		SetWinningTeam(winner);
		if (ABaseGameState* gs = GetGameState<ABaseGameState>())
			gs->AddTeamRound(winner);
	}

	ABaseGameState* gs = GetGameState<ABaseGameState>();
	if (gs && myWinningPlayers.Num() > 0)
		gs->myRoundWinner = myWinningPlayers[0];
}

bool ABaseGameMode::IsWinner(ABasePlayerState* PlayerState) const
{
	return myWinningPlayers.Contains(PlayerState);
}

void ABaseGameMode::GetWinningTeamByPoints()
{
	if (!myModeConfig.myIsTeamBased)
		return;
	ABaseGameState* gs = GetGameState<ABaseGameState>();
	int32 scoreBalance = gs->myTeamScores[CommonTypes::TeamToInt(ETeamID::Team1)] - gs->myTeamScores[CommonTypes::TeamToInt(ETeamID::Team2)];
	if (scoreBalance > 0)
		SetWinningTeam(ETeamID::Team1);
	else if (scoreBalance < 0)
		SetWinningTeam(ETeamID::Team2);
}

void ABaseGameMode::GetWinningPlayerByPoints()
{
	if (myModeConfig.myIsTeamBased)
		return;
	int32 highestScore = 0;
	ABasePlayerState* bestPlayer = nullptr;
	ABaseGameState* gs = GetGameState<ABaseGameState>();
	for (APlayerState* player : gs->PlayerArray)
	{
		ABasePlayerState* basePlayer = Cast<ABasePlayerState>(player);
		if (basePlayer && basePlayer->Score > highestScore)
		{
			highestScore = basePlayer->Score;
			bestPlayer = basePlayer;
		}
	}
	SetWinningPlayer(*bestPlayer);
}

void ABaseGameMode::GetWinningTeamByRounds()
{
	if (!myModeConfig.myIsTeamBased)
		return;
	ABaseGameState* gs = GetGameState<ABaseGameState>();
	int32 roundBalance = gs->myTeamRounds[CommonTypes::TeamToInt(ETeamID::Team1)] - gs->myTeamRounds[CommonTypes::TeamToInt(ETeamID::Team2)];
	if (roundBalance > 0)
		SetWinningTeam(ETeamID::Team1);
	else if (roundBalance < 0)
		SetWinningTeam(ETeamID::Team2);
}

void ABaseGameMode::OnPlayerScoreChanged(ABasePlayerState * Player, int32 Score)
{
	if (myModeConfig.myIsTeamBased || !Player)
		return;

	if (myModeConfig.myWinCondition == EWinCondition::Points && Score >= myModeConfig.myWinThreshold)
		FinishRound();
}

void ABaseGameMode::OnTeamScoreChanged(ETeamID Team, int32 Score)
{
	if (!myModeConfig.myIsTeamBased || !CommonTypes::IsParticipating(Team))
		return;

	if (myModeConfig.myWinCondition == EWinCondition::Points && Score >= myModeConfig.myWinThreshold)
		FinishRound();
}

void ABaseGameMode::OnTeamRoundsChanged(ETeamID Team, int32 Rounds)
{
	if (!myModeConfig.myIsTeamBased || !CommonTypes::IsParticipating(Team))
		return;

	if (myModeConfig.myWinCondition == EWinCondition::Rounds && Rounds >= myModeConfig.myWinThreshold)
		FinishMatch();
}

void ABaseGameMode::OnAlivePlayerCountChanged()
{
	if (myModeConfig.myIsTeamBased 
		&& myModeConfig.myWinCondition == EWinCondition::Rounds
		&& IsATeamDead() != ETeamID::Invalid)
	{
		FinishRound();
	}
}

void ABaseGameMode::SetDetailedMatchState(EMatchState DetailedState, bool CalledByBasicState /*= false*/)
{
	check(DetailedState != EMatchState::InProgress);

	if (ABaseGameState* baseGameState = GetGameState<ABaseGameState>())
	{
		baseGameState->SetDetailedMatchState(DetailedState);

		int32 nextTimeChunk = GetTimeChunkFromState(DetailedState);
		baseGameState->myRemainingTime = nextTimeChunk;
	}

	FName matchStateName = EnumToFName("EMatchState", DetailedState);
	if (!CalledByBasicState && GetBasicMatchState(myDetailedMatchState) != GetBasicMatchState(DetailedState))
		SetMatchState(matchStateName);

	myDetailedMatchState = DetailedState;
}

TArray<ABasePlayerState*> ABaseGameMode::GetWinningPlayers()
{
	return myWinningPlayers;
}

EMatchState ABaseGameMode::ToDetailedMatchState(FName BasicState)
{
	if (BasicState == MatchState::Aborted)
		return EMatchState::Aborted;
	if (BasicState == MatchState::EnteringMap)
		return EMatchState::EnteringMap;
	if (BasicState == MatchState::InProgress)
		return EMatchState::PrepareRound;
	if (BasicState == MatchState::LeavingMap)
		return EMatchState::LeavingMap;
	if (BasicState == MatchState::WaitingPostMatch)
		return EMatchState::WaitingPostMatch;
	if (BasicState == MatchState::WaitingToStart)
		return EMatchState::WaitingToStart;
	if (BasicState == "Warmup")
		return EMatchState::Warmup;
	if (BasicState == "FinishingRound")
		return EMatchState::FinishingRound;
	check(false);
	return EMatchState::Invalid;
}

bool ABaseGameMode::IsBasicMatchState(EMatchState DetailedState)
{
	FName enumName = EnumToFName("EMatchState", DetailedState);
	return IsBasicMatchState(enumName);
}

bool ABaseGameMode::IsBasicMatchState(FName StateName)
{
	return StateName == MatchState::Aborted
		|| StateName == MatchState::EnteringMap
		|| StateName == MatchState::InProgress
		|| StateName == MatchState::LeavingMap
		|| StateName == MatchState::WaitingPostMatch
		|| StateName == MatchState::WaitingToStart;
}

FName ABaseGameMode::GetBasicMatchState(EMatchState DetailedState)
{
	switch (DetailedState)
	{
	case EMatchState::Invalid:
	case EMatchState::Aborted:
		return MatchState::Aborted;
	case EMatchState::EnteringMap:
		return MatchState::EnteringMap;
	case EMatchState::InProgress:
	case EMatchState::FinishingRound:
	case EMatchState::InRound:
	case EMatchState::PrepareRound:
	case EMatchState::Warmup:
		return MatchState::InProgress;
	case EMatchState::LeavingMap:
		return MatchState::LeavingMap;
	case EMatchState::WaitingToStart:
		return MatchState::WaitingToStart;
	case EMatchState::WaitingPostMatch :
		return MatchState::WaitingPostMatch;
	default:
		check(false);
		return MatchState::Aborted;
	}
}

void ABaseGameMode::SendMessage(FMessage Message)
{
	if (UOnlineGameInstance* instance = Cast<UOnlineGameInstance>(GetGameInstance()))
	{
		for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
		{
			if (ABasePlayerController* controller = Cast<ABasePlayerController>(*It))
			{
				if(CanReceiveMessage(controller, Message))
					controller->Client_ReceiveMessage(Message);
			}
		}
	}
}

void ABaseGameMode::HandleMatchIsWaitingToStart()
{
	Super::HandleMatchIsWaitingToStart();

}

void ABaseGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		if(ABasePlayerController* controller = Cast<ABasePlayerController>(*It))
		{
			controller->Client_NotifyMatchStarted();
		}
	}
}

void ABaseGameMode::HandleMatchHasEnded()
{
	Super::HandleMatchHasEnded();

	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		if (ABasePlayerController* controller = Cast<ABasePlayerController>(*It))
		{
			controller->Client_NotifyMatchEnded();
		}
	}
}

void ABaseGameMode::StartMatch()
{
	Super::StartMatch();

	myWinningPlayers.Empty();
	if (ABaseGameState* const gs = Cast<ABaseGameState>(GameState))
		gs->myMatchWinner = nullptr;

	PrepareRound();
}

void ABaseGameMode::StartPlay()
{
	Super::StartPlay();
}

void ABaseGameMode::Reset()
{
	Super::Reset();
}

void ABaseGameMode::StartToLeaveMap()
{
}

void ABaseGameMode::SetMatchState(FName NewState)
{
	Super::SetMatchState(NewState);

	SetDetailedMatchState(ToDetailedMatchState(NewState), true);
}

bool ABaseGameMode::AllowCheats(APlayerController* P)
{
	return myModeConfig.myEnableCheats;
}

void ABaseGameMode::PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	ABaseGameState* const gs = Cast<ABaseGameState>(GameState);
	const bool bMatchIsOver = gs && gs->HasMatchEnded();
	if (bMatchIsOver)
		ErrorMessage = TEXT("Match is over!");
	else
		Super::PreLogin(Options, Address, UniqueId, ErrorMessage);
}

void ABaseGameMode::PostLogin(APlayerController* NewPlayer)
{
	ABasePlayerController* controller = Cast<ABasePlayerController>(NewPlayer);
	ABasePlayerState* playerState = Cast<ABasePlayerState>(controller->PlayerState);
	ABaseGameState* gs = GetGameState<ABaseGameState>();

	if (playerState->GetTeam() == ETeamID::Invalid)
		AutoAssignTeam(playerState);

	if (controller && IsMatchInProgress() && gs)
	{
		controller->Client_NotifyStartOnlineGame();
		controller->Client_NotifyMatchStarted();
		//TODO need to discern at which point in the match we are
		controller->Client_NotifyRoundStart();
	}

	// TODO player transition to spectate or play depending on match state

	Super::PostLogin(NewPlayer);

	myOnPlayerJoined.Broadcast(playerState);
}

void ABaseGameMode::Logout(AController * ExitingPlayer)
{
	Super::Logout(ExitingPlayer);

	myOnPlayerLeft.Broadcast(Cast<ABasePlayerState>(ExitingPlayer->PlayerState));

	if(!CanContinueMatch())
		SetDetailedMatchState(EMatchState::WaitingPostMatch);
}

void ABaseGameMode::AutoAssignTeam(ABasePlayerState * Player)
{
	if (CommonTypes::IsAssigned(Player->GetTeam()))
		return;

	if (!myModeConfig.myIsTeamBased)
	{
		Player->SetTeam(ETeamID::None);
		return;
	}

	int32 teamBalance = 0;
	for (int i = 0; i < GetNumPlayers(); i++)
	{
		if (ABasePlayerState* otherPlayer = Cast<ABasePlayerState>(GameState->PlayerArray[i]))
		{
			switch (otherPlayer->GetTeam())
			{
			case ETeamID::Team1:
				teamBalance--;
				break;
			case ETeamID::Team2:
				teamBalance++;
			default:
				break;
			}
		}
	}
	if (teamBalance <= 0)
		Player->SetTeam(ETeamID::Team2);
	else
		Player->SetTeam(ETeamID::Team1);
}

void ABaseGameMode::SetWinningTeam(ETeamID TeamID)
{
	myWinningPlayers.Empty();
	for (APlayerState* player : GameState->PlayerArray)
	{
		ABasePlayerState* basePlayer = Cast<ABasePlayerState>(player);
		if (basePlayer && basePlayer->GetTeam() == TeamID)
			myWinningPlayers.Add(basePlayer);
	}
}

void ABaseGameMode::SetWinningPlayer(ABasePlayerState& Player)
{
	myWinningPlayers.Empty();
	myWinningPlayers.Add(&Player);
}

void ABaseGameMode::EndCurrentDetailedMatchState()
{
	switch (myDetailedMatchState)
	{
	case EMatchState::WaitingPostMatch:
		RestartGame();
		return;
	case EMatchState::WaitingToStart:
		if (ReadyToStartMatch())
			StartMatch();
		else
			SetDetailedMatchState(EMatchState::Warmup);
		return;
	case EMatchState::FinishingRound:
		RestartRound();
		return;
	case EMatchState::InRound:
		FinishRound();
		return;
	case EMatchState::PrepareRound:
		StartRound();
		return;
	case EMatchState::EnteringMap:
	case EMatchState::LeavingMap:
	case EMatchState::Invalid:
		return;
	default:
		check(false);
		return;
	}
}

int32 ABaseGameMode::GetTimeChunkFromState(EMatchState DetailedState)
{
	switch (DetailedState)
	{
	case EMatchState::WaitingToStart:
		break;
	case EMatchState::WaitingPostMatch:
		return myModeConfig.myMatchRestartTime;
	case EMatchState::PrepareRound:
		return myModeConfig.myRoundPreparationTime;
	case EMatchState::InRound:
		return myModeConfig.myRoundTime;
	case EMatchState::FinishingRound:
		return myModeConfig.myTimeBetweenRounds;
	case EMatchState::InProgress:
		check(false);
	default:
		return 0;
	}
	return 0;
}

ETeamID ABaseGameMode::IsATeamDead()
{
	int32 teamAAlive = 0;
	int32 teamBAlive = 0;
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ABasePlayerController* controller = Cast<ABasePlayerController>(*It);
		ABasePlayerState* playerState = Cast<ABasePlayerState>(controller->PlayerState);
		ABaseCharacter* character = Cast<ABaseCharacter>(controller->GetPawn());

		if (character && character->IsAlive() && playerState)
		{
			if (playerState->GetTeam() == ETeamID::Team1)
				teamAAlive++;
			else if (playerState->GetTeam() == ETeamID::Team2)
				teamBAlive++;
		}
	}

	if (teamAAlive == 0)
		return ETeamID::Team1;
	if (teamBAlive == 0)
		return ETeamID::Team2;
	return ETeamID::Invalid;
}

bool ABaseGameMode::CanReceiveMessage(ABasePlayerController * Player, FMessage Message) const
{
	if (Message.myRecipient < 0 || (Message.myRecipient > 0 && Message.myRecipient != Player->GetUniqueID()))
		return false;

	ABasePlayerState* playerState = Cast<ABasePlayerState>(Player->PlayerState);
	if(CommonTypes::IsAssigned(Message.myTeamFilter) && (!playerState || Message.myTeamFilter != playerState->GetTeam()))
		return false;

	return true;
}

bool ABaseGameMode::CanDamage(const ABasePlayerState* Player, const ABasePlayerState* OtherPlayer) const
{
	if (Player == OtherPlayer && !myModeConfig.myAllowSelfDamage)
		return false;

	check(CommonTypes::IsParticipating(Player->GetTeam()));
	if (myModeConfig.myIsTeamBased && Player->GetTeam() == OtherPlayer->GetTeam() && !myModeConfig.myFriendlyFire)
		return false;

	return true;
}

bool ABaseGameMode::IsSpawnpointAllowed(APlayerStart* SpawnPoint, AController* Player) const
{
	if (Player)
	{
		ABasePlayerState* playerState = Cast<ABasePlayerState>(Player->PlayerState);
		ATeamPlayerStart* playerStart = Cast<ATeamPlayerStart>(SpawnPoint);

		check(CommonTypes::IsParticipating(playerState->GetTeam()));
		if (myModeConfig.myIsTeamBased && playerState && playerStart && playerStart->myAllowedTeam != playerState->GetTeam())
		{
			return false;
		}
	}

	return true;
}

AActor* ABaseGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	TArray<APlayerStart*> starts;
	ABasePlayerState* playerState = Cast<ABasePlayerState>(Player->PlayerState);
	ETeamID playerTeam = playerState->GetTeam();

	if (myModeConfig.myIsTeamBased)
		starts = GetPlayerStarts(playerTeam);

	if (starts.Num() <= 0)
		starts = GetPlayerStarts();

	if (starts.Num() <= 0)
		return nullptr;

	// TODO might need a check here if start is already taken
	return starts[FMath::Rand() % starts.Num()];
}

float ABaseGameMode::ModifyDamage(float Damage, AActor* DamagedActor, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) const
{
	ABaseCharacter* character = Cast<ABaseCharacter>(DamagedActor);
	if (character && EventInstigator)
	{
		ABasePlayerState* DamagedPlayerState = Cast<ABasePlayerState>(character->APawn::GetPlayerState());
		ABasePlayerState* InstigatorPlayerState = Cast<ABasePlayerState>(EventInstigator->PlayerState);

		if (!CanDamage(InstigatorPlayerState, DamagedPlayerState))
		{
			Damage = 0.0f;
		}

		if (InstigatorPlayerState == DamagedPlayerState)
		{
			Damage *= myModeConfig.mySelfDamageModifier;
		}
	}

	return Damage;
}

void ABaseGameMode::Killed(AController * Killer, AController * KilledPlayer, APawn * KilledPawn, const UDamageType * DamageType, AController* Assist /*= nullptr*/)
{
	ABasePlayerState* killerPlayerState = Killer ? Cast<ABasePlayerState>(Killer->PlayerState) : nullptr;
	ABasePlayerState* victimPlayerState = KilledPlayer ? Cast<ABasePlayerState>(KilledPlayer->PlayerState) : nullptr;
	ABasePlayerState* assistPlayerState = Assist ? Cast<ABasePlayerState>(Assist->PlayerState) : nullptr;

	if (killerPlayerState && victimPlayerState)
	{
		killerPlayerState->ScoreKill(victimPlayerState, myModeConfig.myKillScore);
		killerPlayerState->Client_InformAboutKill(killerPlayerState, DamageType, victimPlayerState);
	}

	if (assistPlayerState && killerPlayerState && victimPlayerState)
	{
		assistPlayerState->ScoreAssist(killerPlayerState, victimPlayerState, myModeConfig.myAssistScore);
	} 
	if (victimPlayerState)
	{
		victimPlayerState->ScoreDeath(killerPlayerState, myModeConfig.myDeathScore);
		victimPlayerState->Multicast_BroadcastDeath(killerPlayerState, DamageType, victimPlayerState, assistPlayerState);
	}

	if(!DoesAllowRespawn())
		OnAlivePlayerCountChanged();
}

void ABaseGameMode::Suicide(AController* KilledPlayer, APawn* KilledPawn, const UDamageType* DamageType)
{
	if(ABasePlayerState* victimPlayerState = KilledPlayer ? Cast<ABasePlayerState>(KilledPlayer->PlayerState) : nullptr)
	{
		victimPlayerState->ScoreSuicide(myModeConfig.mySuicideScore);
		victimPlayerState->Multicast_BroadcastSuicide(victimPlayerState, DamageType);
	}

	if (!DoesAllowRespawn())
		OnAlivePlayerCountChanged();
}

TArray<APlayerStart*> ABaseGameMode::GetPlayerStarts(ETeamID TeamFilter /*= ETeamID::None*/)
{
	TArray<APlayerStart*> playerStarts;

	for (TActorIterator<APlayerStart> It(GetWorld()); It; ++It)
	{
		ATeamPlayerStart* teamPlayerStart = Cast<ATeamPlayerStart>(*It);
		if (teamPlayerStart && teamPlayerStart->IsOccupied())
			continue;
		if(CommonTypes::BelongsToTeam(TeamFilter) && teamPlayerStart && teamPlayerStart->myAllowedTeam == TeamFilter)
			playerStarts.Add(*It);
		else if(CommonTypes::IsParticipating(TeamFilter))
			playerStarts.Add(*It);
	}

	return playerStarts;
}

void ABaseGameMode::RestartGame()
{
	//Super::RestartGame();

	FString mapDirectory = "/Game/Maps/";
	UWorld* world = GetWorld();
	world->ServerTravel(mapDirectory + world->GetName());

	// Could notify players about match restart here
	// or use APlayerController::ClientTravel which is called anyway
}

void ABaseGameMode::FinishMatch()
{
	ABaseGameState* const gs = Cast<ABaseGameState>(GameState);
	if (IsMatchInProgress())
	{
		EndMatch();
		DetermineMatchWinner();

		for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
		{
			ABasePlayerState* player = Cast<ABasePlayerState>((*It)->PlayerState);
			const bool isWinner = IsWinner(player);

			(*It)->GameHasEnded(nullptr, isWinner);
		}

		// lock all pawns
		// pawns are not marked as keep for seamless travel, so we will create new pawns on the next match rather than
		// turning these back on.
		for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
		{
			(*It)->TurnOff();
		}
	}
}

void ABaseGameMode::RequestFinishAndExitToMainMenu()
{
	FinishMatch();

	ABasePlayerController* LocalPrimaryController = nullptr;
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		if(ABasePlayerController* controller = Cast<ABasePlayerController>(*Iterator))
		{
			if (!controller->IsLocalController())
			{
				const FText RemoteReturnReason = NSLOCTEXT("NetworkErrors", "HostHasLeft", "Host has left the game.");
				controller->ClientReturnToMainMenuWithTextReason(RemoteReturnReason);
			}
			else
			{
				LocalPrimaryController = controller;
			}
		}
	}

	// GameInstance should be calling this from an EndState.  So call the PC function that performs cleanup, not the one that sets GI state.
	if (LocalPrimaryController)
	{
		LocalPrimaryController->HandleReturnToMainMenu();
	}
}

EMatchState ABaseGameMode::GetDetailedMatchState() const
{
	return myDetailedMatchState;
}

void ABaseGameMode::FinishRound()
{
	SetDetailedMatchState(EMatchState::FinishingRound);

	DetermineRoundWinner();

	// Send end round events
	ABaseGameState* const gs = Cast<ABaseGameState>(GameState);
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		if (ABasePlayerController* controller = Cast<ABasePlayerController>(*It))
		{
			ABasePlayerState* player = Cast<ABasePlayerState>((*It)->PlayerState);
			controller->Client_NotifyRoundEnd(IsWinner(player), gs->ElapsedTime);
			controller->NotifyRoundEnd();
		}
	}
}

void ABaseGameMode::StartRound()
{
	myWinningPlayers.Empty();
	if (ABaseGameState* const gs = Cast<ABaseGameState>(GameState))
		gs->myRoundWinner = nullptr;

	SetDetailedMatchState(EMatchState::InRound);

	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		if (ABasePlayerController* controller = Cast<ABasePlayerController>(*It))
		{
			controller->Client_NotifyRoundStart();
		}
	}
}

void ABaseGameMode::RestartRound()
{
	PrepareRound();
}

void ABaseGameMode::PrepareRound()
{
	SetDetailedMatchState(EMatchState::PrepareRound);

	ResetLevel();

	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ABasePlayerController* controller = Cast<ABasePlayerController>(*It);
		if (controller && !controller->PlayerState->bIsSpectator)
		{
			if (auto start = ChoosePlayerStart(controller))
			{
				FActorSpawnParameters params;
				params.Instigator = nullptr;
				params.Owner = this;
				params.Name = "PlayerCharacter";
				params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
				//TODO modify class by loadout
				FTransform transform;
				transform.SetLocation(start->GetActorLocation());
				transform.SetRotation(start->GetActorRotation().Quaternion());
				ABaseCharacter* newCharacter = GetWorld()->SpawnActor<ABaseCharacter>(DefaultPawnClass, transform, params);
				controller->Possess(newCharacter);

				//TODO loadout selection
				newCharacter->SpawnLoadout();
			}
			UE_LOG(LogTemp, Warning, TEXT("Was not able to find a valid playerstart."));
		}
	}
}

bool ABaseGameMode::DoesAllowRespawn() const
{
	return myModeConfig.myAllowRespawn;
}
