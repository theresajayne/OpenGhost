#include "BPFunctionLibrary.h"
#include "Misc/Paths.h"
#include "HAL/FileManager.h"

TArray<FString> UBPFunctionLibrary::FindFileNames(FString Directory, FString FileType)
{
	TArray<FString> Files;

	if (Directory.Len() < 1)
		return Files;

	FPaths::NormalizeDirectoryName(Directory);

	IFileManager& FileManager = IFileManager::Get();

	if (FileType == "")
	{
		FileType = "*";
	}

	FString FinalPath = FPaths::ProjectContentDir() + Directory + "/*." + FileType;
	FileManager.FindFiles(Files, *FinalPath, true, false);
	
	for (FString& file : Files)
	{
		file.RemoveFromEnd("." + FileType);
	}

	return Files;
}
